package com.darryring.libcore.core;

/**
 * Created by hljdrl on 16/11/4.
 */

public class MessageEvent {

    public final String message;
    public final int what;

    public MessageEvent(String message) {
        this.message = message;
        what = 0;
    }
    public MessageEvent(String message,int _what) {
        this.message = message;
        what = _what;
    }
}
