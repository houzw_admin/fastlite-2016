
Android快速开发框架
======================================== 
2017-03月 更新内容
增加SunActivity页面
![输入图片说明](art/sun_activity.jpg "在这里输入图片标题")
==================================================
2016-11-08 更新内容--start

### [2016-11-08-APK下载地址](art/base_fastlite_v20161108.apk)

FlycoTabHomeActivity 的tabbar 是用到https://github.com/H07000223/FlycoTabLayout 库中的CommonTabLayout 组件
FastTabHomeActivity 的tabbar 是用到CommonTabLayout源码, 对图片和字体 增加了 ColorStateList和 DrawableCompat.setTintList(wrappedDrawable, colors); 主要是实现 图标变色和字体变色(跟随当前主题,图标只有一种颜色,DrawableCompat.setTintList 实现了图片变色)

![FlycoTabHomeActivity](art/FlycoTabHomeActivity_FastTabHomeActivity.jpeg "增加 FlycoTabHomeActivity")


2016-11-08 更新内容--end
==================================================
### [APK下载地址](art/base_fastlite_v1.0.0.apk)

![输入图片说明](art/art_theme_001.jpg "在这里输入图片标题")
![输入图片说明](art/art_theme_002.jpg "在这里输入图片标题")
![输入图片说明](art/art_theme_003.jpg "在这里输入图片标题")
![输入图片说明](art/art_theme_004.jpg "在这里输入图片标题")

===================
 ![输入图片说明](http://git.oschina.net/hljdrl/fastlite/raw/master/art/screenshot_all.gif "在这里输入图片标题")


fastlite 是一个Java抽象代码设计，目前代码分为：抽象层、实现层、调用层

----------
```
核心入口Fast.java
-------------
> **Note:**

package com.darryring.libcore;

import com.darryring.libcore.cache.DiskCache;
import com.darryring.libcore.cache.KVCache;
import com.darryring.libcore.file.Files;
import com.darryring.libcore.http.Http;
import com.darryring.libcore.logger.Logger;
import com.darryring.libcore.media.Media;
import com.darryring.libcore.task.Task;
import com.darryring.libcore.util.NetWork;

/**
 * Created by hljdrl on 15/12/11.
 */
public class Fast {

    //==========================================================
    public static final String VERSION  =    "1.0.0"         ;
    public static final String NAME     =    "FAST-LITE"     ;
    public static final String TIME     =    "2016-06"          ;
    //==========================================================

    public static Logger          logger                ;

    public static KVCache         kvCache               ;

    public static DiskCache       diskCache             ;
     
    public static Files           files                 ;

    public static Media           media                 ;

    public static Http            http                  ;
    
    public static Task            task                  ;

    public static NetWork         network               ;

}

//=========
```


 