package com.darryring.libcore.util;

/**
 * Created by hljdrl on 15/12/18.
 */
public interface LiteCallBack<T> {


    public void onSuccess(T t);

    public void onFailure(T t);
}
