package com.libchat.easemob;

import android.content.Context;

import com.darryring.libchat.ChatManager;
import com.darryring.libchat.ContactManager;
import com.darryring.libchat.FastChat;
import com.darryring.libchat.call.OkCall;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hljdrl on 16/7/12.
 */
public class EasemobChat extends FastChat<Context> {


    private EasemobChatManager     chatManager     = null;
    private EasemobSessionManager  sessionManager  = null;
    private EasemobContactManager  contactManager  = null;

    private Map<String,String> CONFIG = new HashMap<String,String>();
    private boolean mInited = false;

    @Override
    protected void postRunnable(Runnable _runnable) {

    }

    /**
     * @param o
     */
    @Override
    public void init(Context o) {
        chatManager     = new EasemobChatManager     () ;
        sessionManager  = new EasemobSessionManager  () ;
        contactManager  = new EasemobContactManager  () ;
    }




    /**
     * @param k
     * @param v
     */
    @Override
    protected void setConfig(String k, String v) {
        CONFIG.put(k,v);
    }

    /**
     * @param k
     * @return
     */
    @Override
    protected String getConfig(String k) {
        return CONFIG.get(k);
    }

    @Override
    public boolean isOnline() {
        return false;
    }

    /**
     * @param user
     * @param pwd
     */
    @Override
    public void login(String user, String pwd) {

    }

    /**
     * @param user
     * @param pwd
     * @param _call
     */
    @Override
    public void loginAsyn(String user, String pwd, OkCall _call) {

    }

    /**
     *
     */
    @Override
    public void logout() {

    }

    /**
     * @param _call
     */
    @Override
    public void logout(OkCall _call) {

    }

    /**
     * @return
     */
    @Override
    public ContactManager contactManager() {
        return contactManager;
    }

    /**
     * @return
     */
    @Override
    public ChatManager chatManager() {
        return chatManager;
    }
}
