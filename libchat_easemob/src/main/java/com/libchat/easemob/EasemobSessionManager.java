package com.libchat.easemob;

import com.darryring.libchat.SessionManager;

import java.util.List;

/**
 * Created by hljdrl on 16/7/12.
 */
 class EasemobSessionManager implements SessionManager {
    @Override
    public void init() {

    }

   /**
    *
    */
   @Override
   public void loadAllSession() {

   }

   /**
     * 获得会话列表
     *
     * @return
     */
    @Override
    public List getSessionList() {
        return null;
    }

    /**
     * 获得一个会话对象
     *
     * @param _chatUserId
     * @param group
     * @return
     */
    @Override
    public Object getSession(String _chatUserId, boolean group) {
        return null;
    }

    /**
     * Post一个回话消息
     *
     * @param o
     * @param _noreadCount
     */
    @Override
    public void postSession(Object o, int _noreadCount) {

    }

    /**
     * 重置回话中未读消息总数
     *
     * @param o
     */
    @Override
    public void resetSessionMessageCount(Object o) {

    }

    @Override
    public void deleteSession(Object o) {

    }
}
